Controls:
LPM - Place Vertex / Move Selected Vertex
RPM - Select Vertex/ Edge

Buttons:
Clean - Removes the polygon
Finish - Closes the polygonal chain creating a polygon
Rectangle - Creates precoded rectangle
Something - Creates a precoded polygon wuth 6 verticies
Eq. Triangle - Creates a precodeed equal triangle

Delete - Deletes selected vertex

Dissect - splits selected Edge in half
Set Horizontal - sets selected Edge Horizontal
Set Vertical - sets selected Edge Horizontal
Set Length - opens the set length dialog initialized with current length (enter saves the value)

Reordering algorythm:
Given a vertex v0 changed position algorythm will take edge to the next vertex v1 and the end edge to vertex v2. Than depending on edge types will try to place v1 vertex so that all constrains are kept.
If the pair contains a SimpleEdge - the other constrain is taken;
If its a VerticalEdge & HorizontalEdge pair - their intersection is taken;
If the pair contains a ConstantLengthEdge - if there exist a location for v1 that is Length distance away from v2 and it validates the other constrain - the location is chosen, if it doesnt - v2 is moved and the algorytm is run for the next edge.
The algorythm is repeated in both directed fromm the moved vertex v0.

Notes:
1) It is posible to broke the polygon -  for example by creating a triangle and setting lengths not following the Triangle inequality law. A frendly message ("Something is wrong. Make sure you didnt broke the polygon") will be displayed and futher behaviour of this polygon is undefined.
2) As edge length plays a vital role in reordering and painting proces, after each reordering each pair of verticies (complexity O(n^2)) is checked if they arent in the same position - if so one of them will move to new position of (v.x+1,v.y+1).