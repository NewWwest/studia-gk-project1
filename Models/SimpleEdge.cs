﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolygonEditor.Models
{
    public class SimpleEdge : Edge
    {
        public override string Name => name + "S";

        public override bool PlaceEnd()
        {
            return true;
        }

        public override bool PlaceStart()
        {
            return true;
        }
    }
}
