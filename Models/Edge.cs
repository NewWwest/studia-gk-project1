﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolygonEditor.Models
{
    public abstract class Edge
    {
        public Point V1 => Start.Location;

        public Point V2 => End.Location;

        public bool IsSelected { get; set; }

        public Vertex Start { get; set; }

        public Vertex End { get; set; }

        protected string name;

        abstract public string Name { get; }
        abstract public bool PlaceEnd();
        abstract public bool PlaceStart();

        public Edge()
        {
            name = new Random().Next(0, 999).ToString();
        }

        public VerticalEdge ToVertical()
        {
            var edge =  new VerticalEdge()
            {
                IsSelected = IsSelected,
                Start = Start,
                End = End,
                name = name
            };
            Start.Forward = edge;
            End.Back = edge;
            return edge;
        }
        public HorizontalEdge ToHorizontal()
        {
            var edge = new HorizontalEdge()
            {
                IsSelected = IsSelected,
                Start = Start,
                End = End,
                name = name
            };
            Start.Forward = edge;
            End.Back = edge;
            return edge;

        }
        public SimpleEdge ToSimple()
        {
            var edge = new SimpleEdge()
            {
                IsSelected = IsSelected,
                Start = Start,
                End = End,
                name = name
            };
            Start.Forward = edge;
            End.Back = edge;
            return edge;

        }
        public ConstantLengthEdge ToConstantLength(int length)
        {
            var edge = new ConstantLengthEdge()
            {
                IsSelected = IsSelected,
                Start = Start,
                End = End,
                name = name,
                Length = length
            };
            Start.Forward = edge;
            End.Back = edge;
            return edge;

        }
    }
}
