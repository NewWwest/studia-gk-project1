﻿using PolygonEditor.Utils;
using System;
using System.Drawing;

namespace PolygonEditor.Models
{
    public class HorizontalEdge : Edge
    {
        public override string Name => name + "H";

        public override bool PlaceEnd()
        {
            switch (this.End.Forward)
            {
                case SimpleEdge nextEdge:
                    var closest = new Point(nextEdge.Start.Location.X, this.Start.Location.Y);
                    End.Location = closest;
                    return true;
                case VerticalEdge nextEdge:
                    var intersection = new Point(nextEdge.End.Location.X, this.Start.Location.Y);
                    End.Location = intersection;
                    return true;
                case HorizontalEdge nextEdge:
                    throw new NotSupportedException();
                case ConstantLengthEdge nextEdge:
                    var result = ConstantLengthEdgeUtils.TryFindSolutionForHorizontal(this.Start.Location, nextEdge.End.Location, nextEdge.Length, out var endPoint);
                    End.Location = endPoint;
                    return result;
                default:
                    throw new NotSupportedException();
            }
        }

        public override bool PlaceStart()
        {
            switch (this.Start.Back)
            {
                case SimpleEdge prevEdge:
                    var closest = new Point(prevEdge.End.Location.X, this.End.Location.Y);
                    this.Start.Location = closest;
                    return true;
                case VerticalEdge prevEdge:
                    var intersection = new Point(prevEdge.Start.Location.X, this.End.Location.Y);
                    this.Start.Location = intersection;
                    return true;
                case HorizontalEdge prevEdge:
                    throw new NotSupportedException();
                case ConstantLengthEdge prevEdge:
                    var result = ConstantLengthEdgeUtils.TryFindSolutionForHorizontal(this.End.Location, prevEdge.Start.Location, prevEdge.Length, out var endPoint);
                    Start.Location = endPoint;
                    return result;
                default:
                    throw new NotSupportedException();
            }

        }
    }
}
