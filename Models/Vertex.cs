﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolygonEditor.Models
{
    public class Vertex
    {
        public Point Location { get; set; }

        public bool IsSelected { get; set; }

        public Edge Forward { get; set; }

        public Edge Back { get; set; }

        public string Name { get; set; }

        public Vertex Next => Forward.End;

        public Vertex Previous => Back.Start;

        public Vertex()
        {
            Name = new Random().Next(0, 999).ToString();
        }
    }
}
