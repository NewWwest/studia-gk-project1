﻿using PolygonEditor.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolygonEditor.Models
{
    public class ConstantLengthEdge : Edge
    {
        public int Length { get; set; }

        public override string Name => name + "L" + Length.ToString();

        public override bool PlaceEnd()
        {
            switch (this.End.Forward)
            {
                case SimpleEdge nextEdge:

                    var closest = Config.RandomIsFun
                        ? ConstantLengthEdgeUtils.GetRandomPointOnCircle(this.Start.Location, Length)
                        : ConstantLengthEdgeUtils.GetPointOnCircleClosestToXY(this.Start.Location, Length, End.Location);
                    End.Location = closest;
                    return true;
                case VerticalEdge nextEdge:
                    var resultV = ConstantLengthEdgeUtils.TryFindSolutionForVertical(nextEdge.End.Location, this.Start.Location, this.Length, out var endPointV);
                    this.End.Location = endPointV;
                    return resultV;
                case HorizontalEdge nextEdge:
                    var resultH = ConstantLengthEdgeUtils.TryFindSolutionForHorizontal(nextEdge.End.Location, this.Start.Location, this.Length, out var endPointH);
                    this.End.Location = endPointH;
                    return resultH;
                case ConstantLengthEdge nextEdge:
                    var result_C = ConstantLengthEdgeUtils.PlaceEnd_ConstantLengthEdge(nextEdge, this, Length, out Point endPoint_C);
                    this.End.Location = endPoint_C;
                    return result_C;
                default:
                    throw new NotSupportedException();
            }
        }

        public override bool PlaceStart()
        {
            switch (this.Start.Back)
            {
                case SimpleEdge prevEdge:
                    var closest = Config.RandomIsFun
                        ? ConstantLengthEdgeUtils.GetRandomPointOnCircle(this.End.Location, Length)
                        : ConstantLengthEdgeUtils.GetPointOnCircleClosestToXY(this.End.Location, Length, Start.Location);
                    Start.Location = closest;
                    return true;
                case VerticalEdge prevEdge:
                    var resultV = ConstantLengthEdgeUtils.TryFindSolutionForVertical(prevEdge.Start.Location, this.End.Location, this.Length, out var endPointV);
                    Start.Location = endPointV;
                    return resultV;
                case HorizontalEdge prevEdge:
                    var resultH = ConstantLengthEdgeUtils.TryFindSolutionForHorizontal(prevEdge.Start.Location, this.End.Location, this.Length, out var endPointH);
                    Start.Location = endPointH;
                    return resultH;
                case ConstantLengthEdge prevEdge:
                    var result_C = ConstantLengthEdgeUtils.PlaceStart_ConstantLengthEdge(prevEdge, this, out Point endPoint_C);
                    Start.Location = endPoint_C;
                    return result_C;
                default:
                    throw new NotSupportedException();
            }
        }
    }
}
