﻿using PolygonEditor.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace PolygonEditor.Models
{
    internal class Polygon
    {
        List<Vertex> vertecies = new List<Vertex>();
        bool finishedConstruction = false;
        Logger logger;

        public bool FinishedConstruction
        {
            get => finishedConstruction;
            set
            {
                if (finishedConstruction)
                {
                    logger.Message("You can have only one polygon");
                    return;
                }
                if (vertecies.Count < 3)
                {
                    logger.Message("You cannot have a polygon with less than three sides");
                    return;
                }
                finishedConstruction = true;

                var edge = new SimpleEdge()
                {
                    Start = LastVertex,
                    End = FirstVertex,
                };
                LastVertex.Forward = edge;
                FirstVertex.Back = edge;
                vertecies[0].IsSelected = true;
                logger.Message("Polygon constructed");
            }
        }

        public Polygon(Logger logger)
        {
            this.logger = logger;
        }

        public IEnumerable<Vertex> Vertecies => vertecies;

        public IEnumerable<Edge> Edges
        {
            get
            {
                if (!vertecies.Any())
                    yield break;

                int count = 0;
                Vertex current = vertecies[0];
                while (true)
                {
                    if (count >= vertecies.Count)
                        yield break;
                    if (current.Forward == null)
                        yield break;

                    yield return current.Forward;
                    current = current.Forward.End;
                    count++;
                }
            }
        }

        public Vertex AddVertex(Point location)
        {
            if (finishedConstruction)
            {
                logger.Message("Polygon already created");
                return null ;
            }
            var v = new Vertex()
            {
                Location = location,
                IsSelected = false
            };
            vertecies.Add(v);
            if (vertecies.Count > 1)
            {
                var edge = new SimpleEdge()
                {
                    Start = AlmostLastVertex,
                    End = LastVertex,
                };
                AlmostLastVertex.Forward = edge;
                LastVertex.Back = edge;
            }
            return v;
        }

        internal void SetLength(int length)
        {
            if (!finishedConstruction)
            {
                logger.Message("First finish construction");
                return;
            }
            var selectedEdge = SelectedEdge();
            if (selectedEdge == null)
            {
                logger.Message("You have to have an edge selected");
                return;
            }
            selectedEdge = selectedEdge.ToConstantLength(length);
            logger.Message($"Edge {selectedEdge.Name} set to length {length}");
            Reorder(selectedEdge.Start);

        }
        internal int? GetLength()
        {
            if (!finishedConstruction)
            {
                logger.Message("First finish construction");
                return null;
            }
            var selectedEdge = SelectedEdge();
            if (selectedEdge == null)
            {
                logger.Message("You have to have an edge selected");
                return null;
            }
            return (int)Helper.Distance(selectedEdge.V1, selectedEdge.V2);

        }

        internal void SetVertical()
        {
            if (!finishedConstruction)
            {
                logger.Message("First finish construction");
                return;
            }
            var selectedEdge = SelectedEdge();
            if (selectedEdge == null)
            {
                logger.Message("You have to have an edge selected");
                return;
            }
            if (selectedEdge.Start.Back.GetType() == typeof(VerticalEdge) ||
               selectedEdge.End.Forward.GetType() == typeof(VerticalEdge))
            {
                logger.Message("Neighboring edge cant be both vertical");
                return;
            }

            selectedEdge = selectedEdge.ToVertical();
            logger.Message($"Edge {selectedEdge.Name} set to vertical");
            Reorder(selectedEdge.Start);
        }

        internal void SetHorizontal()
        {
            if (!finishedConstruction)
            {
                logger.Message("First finish construction");
                return;
            }
            var selectedEdge = SelectedEdge();
            if (selectedEdge == null)
            {
                logger.Message("You have to have an edge selected");
                return;
            }
            if (selectedEdge.Start.Back.GetType() == typeof(HorizontalEdge) ||
               selectedEdge.End.Forward.GetType() == typeof(HorizontalEdge))
            {
                logger.Message("Neighboring edge cant be both horizontal");
                return;
            }

            selectedEdge = selectedEdge.ToHorizontal();
            logger.Message($"Edge {selectedEdge.Name} set to horizontal");
            Reorder(selectedEdge.Start);
        }

        public void DisectEdge()
        {
            if (!finishedConstruction)
            {
                logger.Message("First finish construction");
                return;
            }
            var selectedEdge = SelectedEdge();
            if (selectedEdge == null)
            {
                logger.Message("You have to have an edge selected");
                return;
            }

            var midVertex = new Vertex()
            {
                Location = Helper.MidPoint(selectedEdge.V1, selectedEdge.V2),
            };
            vertecies.Add(midVertex);

            var e1 = new SimpleEdge()
            {
                Start = selectedEdge.Start,
                End = midVertex
            };
            var e2 = new SimpleEdge()
            {
                Start = midVertex,
                End = selectedEdge.End
            };
            selectedEdge.Start.Forward = e1;
            midVertex.Back = e1;
            midVertex.Forward = e2;
            selectedEdge.End.Back = e2;
            logger.Message($"Edge {selectedEdge.Name} disected");
        }

        public void Select(Point location)
        {
            double VcurrentMinDistance = double.MaxValue;
            Vertex VcurrentClosest = null;
            foreach (var vertex in vertecies)
            {
                var distance = Helper.Distance(vertex.Location, location);
                if (distance < VcurrentMinDistance)
                {
                    VcurrentMinDistance = distance;
                    VcurrentClosest = vertex;
                }
            }
            if (VcurrentMinDistance < Config.VertexRadius)
            {
                Deselect();
                VcurrentClosest.IsSelected = true;
                logger.Message($"Selected Vertex {VcurrentClosest.Name}");
                return;
            }
            // else select Edge
            double EcurrentMinDistance = double.MaxValue;
            Edge EcurrentClosest = null;
            foreach (var edge in Edges)
            {
                var distance = Helper.Distance(location, edge);
                if (distance < EcurrentMinDistance)
                {
                    EcurrentMinDistance = distance;
                    EcurrentClosest = edge;
                }
            }
            if (EcurrentMinDistance < Config.EdgeHitBox)
            {

                Deselect();
                EcurrentClosest.IsSelected = true;
                logger.Message($"Selected Edge {EcurrentClosest.Name}");
                return;
            }

        }

        public void MoveVertex(Point location)
        {
            var selectedVertex = SelectedVertex();
            if (selectedVertex == null)
            {
                logger.Message("You have to have a vertex selected");
                return;
            }
            selectedVertex.Location = location;
            Reorder(selectedVertex);
        }

        private void Deselect()
        {
            var selectedVertex = SelectedVertex();
            if (selectedVertex != null)
            {
                selectedVertex.IsSelected = false;
                return;
            }
            var selectedEdge = SelectedEdge();
            if (selectedEdge != null)
            {
                selectedEdge.IsSelected = false;
                return;
            }
        }

        private void Reorder(Vertex startVertex)
        {
            Vertex current = startVertex;
            int i = 0;
            while (!current.Forward.PlaceEnd())
            {
                current = current.Next;
                if(i++>vertecies.Count*2)
                {
                    logger.Message($"POLYGON IS BROKEN");
                    return;
                }
            }
            current = startVertex;
            i = 0;
            while (!current.Back.PlaceStart())
            {
                current = current.Previous;
                if (i++ > vertecies.Count*2)
                {
                    logger.Message($"POLYGON IS BROKEN");
                    return;
                }
            }

            RemoveSuperimposedVertecies();
        }

        private void RemoveSuperimposedVertecies()
        {
            for (int i = 0; i < vertecies.Count; i++)
            {
                for (int j = i+1; j < vertecies.Count; j++)
                {
                    if (vertecies[i].Location == vertecies[j].Location)
                    {
                        vertecies[j].Location = new Point(vertecies[j].Location.X + 1, vertecies[j].Location.Y + 1);
                    }
                }
            }
        }

        public void DeleteVertex()
        {
            if (!finishedConstruction)
            {
                logger.Message("You have to have a finished polygon");
                return;
            }


            var selectedVertex = SelectedVertex();
            if (selectedVertex == null)
            {
                logger.Message("You have to have a vertex selected");
                return;
            }
            if (vertecies.Count < 4)
            {
                logger.Message("You have to have at least 3 vertecies left over");
                return;
            }


            var replacementEdge = new SimpleEdge()
            {
                Start = selectedVertex.Back.Start,
                End = selectedVertex.Forward.End
            };
            selectedVertex.Back.Start.Forward = replacementEdge;
            selectedVertex.Forward.End.Back = replacementEdge;
            vertecies.Remove(selectedVertex);

            logger.Message($"Removed vertex {selectedVertex.Name}");
        }

        public Vertex SelectedVertex() => vertecies.FirstOrDefault(v => v.IsSelected);
        private Edge SelectedEdge() => Edges.FirstOrDefault(e => e.IsSelected);
        private Vertex LastVertex => vertecies.Last();
        private Vertex AlmostLastVertex => vertecies[vertecies.Count - 2];
        private Vertex FirstVertex => vertecies.First();




        public string Serialize()
        {
            var data = new StringBuilder();
            Edge first = null;
            foreach (var edge in Edges)
            {
                if (first == null)
                    first = edge;
                data.Append(edge.Start.Location.X.ToString());
                data.Append(";");
                data.Append(edge.Start.Location.Y.ToString());
                data.Append(Environment.NewLine);
                switch (edge)
                {
                    case SimpleEdge e:
                        data.Append("S");
                        data.Append(Environment.NewLine);
                        break;
                    case VerticalEdge e:
                        data.Append("V");
                        data.Append(Environment.NewLine);
                        break;
                    case HorizontalEdge e:
                        data.Append("H");
                        data.Append(Environment.NewLine);
                        break;
                    case ConstantLengthEdge e:
                        data.Append("C"+e.Length.ToString());
                        data.Append(Environment.NewLine);
                        break;
                    default:
                        throw new NotSupportedException();
                }
            }
            return data.ToString();
        }
        public static Polygon Deserialize(string data, Logger logger)
        {
            var polygon = new Polygon(logger);
            string[] lines = data.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            string firstVertex= lines[0];
            int firstVertexX = int.Parse(firstVertex.Split(';')[0]);
            int firstVertexY = int.Parse(firstVertex.Split(';')[1]);
            var v = polygon.AddVertex(new Point(firstVertexX, firstVertexY));
            for (int i = 1; i < lines.Length-1; i+=2)
            {
                string edgeS = lines[i];
                string vertexS = lines[i+1];

                int vertexX = int.Parse(vertexS.Split(';')[0]);
                int vertexY = int.Parse(vertexS.Split(';')[1]);
                var vNext = polygon.AddVertex(new Point(vertexX, vertexY));
                switch (edgeS[0])
                {
                    case 'S':
                        break;
                    case 'V':
                        v.Forward = v.Forward.ToVertical();
                        break;
                    case 'H':
                        v.Forward = v.Forward.ToHorizontal();
                        break;
                    case 'C':
                        int length = int.Parse(edgeS.Substring(1, edgeS.Length - 1));
                        v.Forward = v.Forward.ToConstantLength(length);
                        break;
                    default:
                        throw new IndexOutOfRangeException();
                }
                v = v.Next;
                
            }
            polygon.FinishedConstruction = true;
            switch (lines[lines.Length - 1][0])
            {
                case 'S':
                    break;
                case 'V':
                    v.Forward = v.Forward.ToVertical();
                    break;
                case 'H':
                    v.Forward = v.Forward.ToHorizontal();
                    break;
                case 'C':
                    int length = int.Parse(lines[lines.Length - 1].Substring(1, lines[lines.Length - 1].Length - 1));
                    v.Forward = v.Forward.ToConstantLength(length);
                    break;
                default:
                    throw new IndexOutOfRangeException();
            }
            return polygon;
        }
    }
}