﻿using PolygonEditor.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolygonEditor.Models
{
    public class VerticalEdge : Edge
    {
        public override string Name => name + "V";

        public override bool PlaceEnd()
        {
            switch (this.End.Forward)
            {
                case SimpleEdge nextEdge:
                    var closest = new Point(this.Start.Location.X, nextEdge.Start.Location.Y);
                    End.Location = closest;
                    return true;
                case VerticalEdge nextEdge:
                    throw new NotSupportedException();
                case HorizontalEdge nextEdge:
                    var intersection = new Point(this.Start.Location.X, nextEdge.End.Location.Y);
                    End.Location = intersection;
                    return true;
                case ConstantLengthEdge nextEdge:
                    var result = ConstantLengthEdgeUtils.TryFindSolutionForVertical(this.Start.Location, nextEdge.End.Location, nextEdge.Length, out var endPoint);
                    End.Location = endPoint;
                    return result;
                default:
                    throw new NotSupportedException();
            }

        }

        public override bool PlaceStart()
        {
            switch (this.Start.Back)
            {
                case SimpleEdge prevEdge:
                    var intersection = new Point(this.End.Location.X, prevEdge.End.Location.Y);
                    Start.Location = intersection;
                    return true;
                case VerticalEdge prevEdge:
                    throw new NotSupportedException();
                case HorizontalEdge prevEdge:
                    var closest = new Point(this.End.Location.X, prevEdge.Start.Location.Y);
                    Start.Location = closest;
                    return true;
                case ConstantLengthEdge prevEdge:
                    var result = ConstantLengthEdgeUtils.TryFindSolutionForVertical(this.End.Location, prevEdge.Start.Location, prevEdge.Length, out var endPoint);
                    Start.Location = endPoint;
                    return result;
                default:
                    throw new NotSupportedException();
            }
        }
    }
}
