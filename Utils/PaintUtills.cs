﻿using PolygonEditor.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolygonEditor.Utils
{
    static class PaintUtills
    {
        const string FontName = "Arial";

        public static void PaintPolygon(Bitmap bitmap, float fontToRadiusRatio, Polygon polygon)
        {
            var g = Graphics.FromImage(bitmap);
            int radius = Config.VertexRadius;

            foreach (var edge in polygon.Edges)
            {
                var color = edge.IsSelected
                    ? Config.SelectedEdgeColor
                    : Config.EdgeColor;
                var brush = new SolidBrush(color);

                var pen = new Pen(brush, Config.PenWidth);
                if (Config.UseEmbededLineAlgorythm)
                    g.DrawLine(pen, edge.V1, edge.V2);
                else
                    BresenhamLine(bitmap, color, edge.V1, edge.V2);
                var font = new Font(FontName, (int)(radius * fontToRadiusRatio));
                g.DrawString(edge.Name, font, new SolidBrush(Config.LabelColor), Helper.MidPoint(edge.V1, edge.V2));

            }

            foreach (var vertex in polygon.Vertecies)
            {
                var VertexBackground = vertex.IsSelected ?
                        new SolidBrush(Config.SelectedVertexBackground) :
                        new SolidBrush(Config.VertexBackground);

                var brush = new SolidBrush(Config.VertexColor);
                var pen = new Pen(brush, Config.PenWidth);
                var font = new Font(FontName, (int)(radius * fontToRadiusRatio));
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                StringFormat sf = new StringFormat();
                sf.LineAlignment = StringAlignment.Center;
                sf.Alignment = StringAlignment.Center;
                var Totalrect = new Rectangle(vertex.Location.X - radius - Config.PenWidth, vertex.Location.Y - radius - Config.PenWidth,
                    radius * 2 + 2 * Config.PenWidth, radius * 2 + 2 * Config.PenWidth);
                var rect = new Rectangle(vertex.Location.X - radius, vertex.Location.Y - radius, radius * 2, radius * 2);
                g.FillEllipse(VertexBackground, Totalrect);
                g.DrawEllipse(pen, rect);

                g.DrawString(vertex.Name, font, new SolidBrush(Config.LabelColor), rect, sf);
            }


        }



        public static void PaintPolygonFlood(Bitmap bitmap, float fontToRadiusRatio, Polygon polygon, bool withGuidlines = false, Point mouseLocation = default(Point))
        {
            var g = Graphics.FromImage(bitmap);
            int radius = Config.VertexRadius;
            var whitebrush = new SolidBrush(Color.FromKnownColor(KnownColor.White));

            var Floodrect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            g.FillRectangle(whitebrush, Floodrect);

            PaintPolygon(bitmap, fontToRadiusRatio, polygon);
            if (withGuidlines)
                PaintGuidlines(bitmap, polygon, mouseLocation);
        }

        private static void PaintGuidlines(Bitmap bitmap, Polygon polygon, Point mouseLocation)
        {
            //If finished construction use the selected vertex - othervise use mouselocation
            Vertex v = polygon.SelectedVertex();
            foreach (var vertex in polygon.Vertecies)
            {
                if (polygon.FinishedConstruction && vertex == v)
                    continue;
                Point current = polygon.FinishedConstruction ? v.Location : mouseLocation;
                if (Math.Abs(vertex.Location.X - current.X) < Config.GuideLineHitBox)
                {
                    var g = Graphics.FromImage(bitmap);
                    var color = Config.EdgeColor;
                    var brush = new SolidBrush(color);
                    var pen = new Pen(brush, Config.PenWidth);
                    float[] dashValues = { 4, 2 };
                    pen.DashPattern = dashValues;
                    g.DrawLine(pen, new Point(vertex.Location.X, 1), new Point(vertex.Location.X, bitmap.Width - 1));
                    return;
                }
                if (Math.Abs(vertex.Location.Y - current.Y) < Config.GuideLineHitBox)
                {
                    var g = Graphics.FromImage(bitmap);
                    var color = Config.EdgeColor;
                    var brush = new SolidBrush(color);
                    var pen = new Pen(brush, Config.PenWidth);
                    float[] dashValues = { 4, 2 };
                    pen.DashPattern = dashValues;
                    g.DrawLine(pen, new Point(1, vertex.Location.Y), new Point(bitmap.Height - 1, vertex.Location.Y));
                    return;
                }
            }
        }

        public static void BresenhamLine(Bitmap bitmap, Color color, Point p1, Point p2)
        {
            // zmienne pomocnicze
            int d, dx, dy, ai, bi, xi, yi;
            int x = p1.X;
            int y = p1.Y;
            // ustalenie kierunku rysowania
            if (p1.X < p2.X)
            {
                xi = 1;
                dx = p2.X - p1.X;
            }
            else
            {
                xi = -1;
                dx = p1.X - p2.X;
            }
            // ustalenie kierunku rysowania
            if (p1.Y < p2.Y)
            {
                yi = 1;
                dy = p2.Y - p1.Y;
            }
            else
            {
                yi = -1;
                dy = p1.Y - p2.Y;
            }
            // pierwszy piksel
            bitmap.SetPixel(x, y, color);
            // oś wiodąca OX
            if (dx > dy)
            {
                ai = (dy - dx) * 2;
                bi = dy * 2;
                d = bi - dx;
                // pętla po kolejnych x
                while (x != p2.X)
                {
                    // test współczynnika
                    if (d >= 0)
                    {
                        x += xi;
                        y += yi;
                        d += ai;
                    }
                    else
                    {
                        d += bi;
                        x += xi;
                    }
                    bitmap.SetPixel(x, y, color);
                }
            }
            // oś wiodąca OY
            else
            {
                ai = (dx - dy) * 2;
                bi = dx * 2;
                d = bi - dy;
                // pętla po kolejnych y
                while (y != p2.Y)
                {
                    // test współczynnika
                    if (d >= 0)
                    {
                        x += xi;
                        y += yi;
                        d += ai;
                    }
                    else
                    {
                        d += bi;
                        y += yi;
                    }
                    bitmap.SetPixel(x, y, color);
                }
            }
        }
    }
}
