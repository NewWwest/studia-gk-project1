﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Windows.Forms;
using PolygonEditor.Models;

namespace PolygonEditor.Utils
{
    static class Helper
    {


        static public double Distance(Point a, Point b)
        {
            return Math.Sqrt((a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y));
        }
        static public Point MidPoint(Point v1, Point v2)
        {
            return new Point((v1.X + v2.X) / 2, (v1.Y + v2.Y) / 2);
        }
        static public double Distance(Point pt, Edge edge)
        {
            Point p1 = edge.V1;
            Point p2 = edge.V2;
            float dx = p2.X - p1.X;
            float dy = p2.Y - p1.Y;
            if ((dx == 0) && (dy == 0))
                throw new Exception("Edge of length 0");

            // Calculate the t that minimizes the distance.
            float t = ((pt.X - p1.X) * dx + (pt.Y - p1.Y) * dy) /
                (dx * dx + dy * dy);

            // See if this represents one of the segment's
            // end points or a point in the middle.
            if (t < 0)
            {
                dx = pt.X - p1.X;
                dy = pt.Y - p1.Y;
            }
            else if (t > 1)
            {
                dx = pt.X - p2.X;
                dy = pt.Y - p2.Y;
            }
            else
            {
                Point closest = new Point((int)(p1.X + t * dx), (int)(p1.Y + t * dy));
                dx = pt.X - closest.X;
                dy = pt.Y - closest.Y;
            }

            return Math.Sqrt(dx * dx + dy * dy);
        }


        static public Tuple<Point, Point> SecantPointsOY(Point origin, int r, int x)
        {
            var p1 = origin.Y + Math.Sqrt(r * r - (x - origin.X) * (x - origin.X));
            var p2 = origin.Y - Math.Sqrt(r * r - (x - origin.X) * (x - origin.X));
            return new Tuple<Point, Point>(new Point(x,(int)p1), new Point(x, (int)p2));
        }

        static public Tuple<Point, Point> SecantPointsOX(Point origin, int r, int y)
        {
            var p1 = origin.X + Math.Sqrt(r * r - (y - origin.Y) * (y - origin.Y));
            var p2 = origin.X - Math.Sqrt(r * r - (y - origin.Y) * (y - origin.Y));
            return new Tuple<Point, Point>(new Point((int)p1,y), new Point((int)p2, y));
        }




    }
}
