﻿using PolygonEditor.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolygonEditor.Utils
{
    public static class ConstantLengthEdgeUtils
    {
        
        
        static public bool TryFindSolutionForVertical(Point verticalReferencePoint, Point circleReferencePoint, int Length, out Point EndPoint)
        {
            EndPoint = new Point();
            //1. edge case
            if (Math.Abs(verticalReferencePoint.X - circleReferencePoint.X) == Length)
            {
                var tangentPoint = new Point(verticalReferencePoint.X, circleReferencePoint.Y);
                EndPoint = tangentPoint;
                return true;
            }
            //2. in range
            if (Math.Abs(verticalReferencePoint.X - circleReferencePoint.X) < Length)
            {
                var secantPoints = Helper.SecantPointsOY(circleReferencePoint, Length, verticalReferencePoint.X);
                var closestSecant = Helper.Distance(secantPoints.Item1, verticalReferencePoint) < Helper.Distance(secantPoints.Item2, verticalReferencePoint)
                    ? secantPoints.Item1
                    : secantPoints.Item2;
                EndPoint = closestSecant;
                return true;
            }
            //3. out of range
            int offset = Length;
            if (circleReferencePoint.X > verticalReferencePoint.X)
                offset = -Length;
            var closestToOY = new Point(circleReferencePoint.X + offset, circleReferencePoint.Y);
            EndPoint = closestToOY;
            return false;
        }
        static public bool TryFindSolutionForHorizontal(Point horizontalReferencePoint, Point circleReferencePoint, int Length, out Point EndPoint)
        {
            EndPoint = new Point();
            //1. edge case
            if (Math.Abs(horizontalReferencePoint.Y - circleReferencePoint.Y) == Length)
            {
                var tangentPoint = new Point(circleReferencePoint.X, horizontalReferencePoint.Y);
                EndPoint = tangentPoint;
                return true;
            }
            //2. in range
            if (Math.Abs(horizontalReferencePoint.Y - circleReferencePoint.Y) < Length)
            {
                var secantPoints = Helper.SecantPointsOX(circleReferencePoint, Length, horizontalReferencePoint.Y);
                var closestSecant = Helper.Distance(secantPoints.Item1, horizontalReferencePoint) < Helper.Distance(secantPoints.Item2, horizontalReferencePoint)
                    ? secantPoints.Item1
                    : secantPoints.Item2;
                EndPoint = closestSecant;
                return true;
            }
            //3. out of range
            int offset = Length;
            if (circleReferencePoint.Y > horizontalReferencePoint.Y)
                offset = -Length;
            var closestToOX = new Point(circleReferencePoint.X, circleReferencePoint.Y + offset);
            EndPoint = closestToOX;
            return false;
        }
        static public Point GetPointOnCircleClosestToXY(Point origin, int r, Point p)
        {
            double vX = p.X - origin.X;
            double vY = p.Y - origin.Y;
            double magV = Math.Sqrt(vX * vX + vY * vY);
            double aX = origin.X + vX / magV * r;
            double aY = origin.Y + vY / magV * r;
            return new Point((int)aX, (int)aY);

        }
        static public Point GetRandomPointOnCircle(Point origin, int r)
        {
            int x = origin.X + new Random().Next(-r, r);
            int y = (int)(origin.Y - Math.Sqrt(r * r - (x - origin.X) * (x - origin.X)));
            return new Point(x, y);
        }

        static public bool PlaceStart_ConstantLengthEdge(ConstantLengthEdge prevEdge, ConstantLengthEdge that, out Point EndPoint)
        {
            EndPoint = new Point();
            int originsDistance = (int)Helper.Distance(prevEdge.Start.Location, that.End.Location);
            //2. in range
            if (originsDistance < that.Length + prevEdge.Length)
            {
                var intersectionPoints = IntersectionTwoCircles(that.End.Location, that.Length, prevEdge.Start.Location, prevEdge.Length);
                Point closestSecant;
                if (Config.RandomIsFun)
                    closestSecant = intersectionPoints.Item2; //Randomly chosen by a coin flip
                else
                    closestSecant = Helper.Distance(intersectionPoints.Item1, that.Start.Location) < Helper.Distance(intersectionPoints.Item2, that.Start.Location)
                        ? intersectionPoints.Item1
                        : intersectionPoints.Item2;

                EndPoint = closestSecant;
                return true;
            }
            //3. out of range
            var closestToPrevious = GetPointOnCircleClosestToXY(that.End.Location, that.Length, prevEdge.Start.Location);
            EndPoint = closestToPrevious;
            return false;
        }
        static public bool PlaceEnd_ConstantLengthEdge(ConstantLengthEdge nextEdge, ConstantLengthEdge that, int Length, out Point EndPoint)
        {
            EndPoint = new Point();
            int originsDistance = (int)Helper.Distance(nextEdge.End.Location, that.Start.Location);
            if (originsDistance < Length + nextEdge.Length)
            {
                var intersectionPoints = IntersectionTwoCircles(that.Start.Location, Length, nextEdge.End.Location, nextEdge.Length);
                Point closestSecant;
                if (Config.RandomIsFun)
                    closestSecant = intersectionPoints.Item2; //Randomly chosen by a coin flip
                else
                    closestSecant = Helper.Distance(intersectionPoints.Item1, that.End.Location) < Helper.Distance(intersectionPoints.Item2, that.End.Location)
                        ? intersectionPoints.Item1
                        : intersectionPoints.Item2;

                EndPoint = closestSecant;
                return true;
            }
            var closestToPrevious = GetPointOnCircleClosestToXY(that.Start.Location, Length, nextEdge.End.Location);
            EndPoint = closestToPrevious;
            return false;
        }


        static private Tuple<Point, Point> IntersectionTwoCircles(Point origin1, double r1, Point origin2, double r2)
        {
            var d = Helper.Distance(origin1, origin2);
            var ex = (origin2.X - origin1.X) / d;
            var ey = (origin2.Y - origin1.Y) / d;

            var x = (r1 * r1 - r2 * r2 + d * d) / (2 * d);
            var y = Math.Sqrt(r1 * r1 - x * x);

            Point P1 = new Point()
            {
                X = (int)(origin1.X + x * ex - y * ey),
                Y = (int)(origin1.Y + x * ey + y * ex)
            };

            Point P2 = new Point()
            {
                X = (int)(origin1.X + x * ex + y * ey),
                Y = (int)(origin1.Y + x * ey - y * ex)
            };
            return new Tuple<Point, Point>(P1, P2);
        }
    }
}
