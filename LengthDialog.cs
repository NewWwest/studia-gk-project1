﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolygonEditor
{
    public partial class LengthDialog : Form
    {
        public int Length { get; set; }
        public Action Callback { get; set; }
        public LengthDialog()
        {
            InitializeComponent();
            LengthBox.Select();
            ControlBox = false;
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void OK_Click(object sender, EventArgs e)
        {
            if (int.TryParse(LengthBox.Text, out int length))
                if (length > 0)
                {
                    Length = length;
                    Callback();
                    this.Close();
                }
        }

        private void LengthBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void LengthBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                OK_Click(null, null);
        }

        private void LengthDialog_Shown(object sender, EventArgs e)
        {

            LengthBox.Text = Length.ToString();
        }
    }
}
