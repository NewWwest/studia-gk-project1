﻿namespace PolygonEditor
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Splitter = new System.Windows.Forms.SplitContainer();
            this.Saver = new System.Windows.Forms.Button();
            this.Opener = new System.Windows.Forms.Button();
            this.CreateTriangle = new System.Windows.Forms.Button();
            this.CreateSomethin = new System.Windows.Forms.Button();
            this.CreateRectangle = new System.Windows.Forms.Button();
            this.Finisher = new System.Windows.Forms.Button();
            this.Lengther = new System.Windows.Forms.Button();
            this.Verticaler = new System.Windows.Forms.Button();
            this.Horizonter = new System.Windows.Forms.Button();
            this.Disecter = new System.Windows.Forms.Button();
            this.Deleter = new System.Windows.Forms.Button();
            this.Cleaner = new System.Windows.Forms.Button();
            this.Painter = new System.Windows.Forms.PictureBox();
            this.MessageLog = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.Splitter)).BeginInit();
            this.Splitter.Panel1.SuspendLayout();
            this.Splitter.Panel2.SuspendLayout();
            this.Splitter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Painter)).BeginInit();
            this.SuspendLayout();
            // 
            // Splitter
            // 
            this.Splitter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Splitter.Location = new System.Drawing.Point(0, 0);
            this.Splitter.Name = "Splitter";
            // 
            // Splitter.Panel1
            // 
            this.Splitter.Panel1.Controls.Add(this.Saver);
            this.Splitter.Panel1.Controls.Add(this.Opener);
            this.Splitter.Panel1.Controls.Add(this.CreateTriangle);
            this.Splitter.Panel1.Controls.Add(this.CreateSomethin);
            this.Splitter.Panel1.Controls.Add(this.CreateRectangle);
            this.Splitter.Panel1.Controls.Add(this.Finisher);
            this.Splitter.Panel1.Controls.Add(this.Lengther);
            this.Splitter.Panel1.Controls.Add(this.Verticaler);
            this.Splitter.Panel1.Controls.Add(this.Horizonter);
            this.Splitter.Panel1.Controls.Add(this.Disecter);
            this.Splitter.Panel1.Controls.Add(this.Deleter);
            this.Splitter.Panel1.Controls.Add(this.Cleaner);
            // 
            // Splitter.Panel2
            // 
            this.Splitter.Panel2.Controls.Add(this.Painter);
            this.Splitter.Size = new System.Drawing.Size(1382, 653);
            this.Splitter.SplitterDistance = 138;
            this.Splitter.TabIndex = 0;
            // 
            // Saver
            // 
            this.Saver.Location = new System.Drawing.Point(13, 187);
            this.Saver.Name = "Saver";
            this.Saver.Size = new System.Drawing.Size(109, 23);
            this.Saver.TabIndex = 11;
            this.Saver.Text = "Saver";
            this.Saver.UseVisualStyleBackColor = true;
            this.Saver.Click += new System.EventHandler(this.Saver_Click);
            // 
            // Opener
            // 
            this.Opener.Location = new System.Drawing.Point(13, 158);
            this.Opener.Name = "Opener";
            this.Opener.Size = new System.Drawing.Size(109, 23);
            this.Opener.TabIndex = 10;
            this.Opener.Text = "Open";
            this.Opener.UseVisualStyleBackColor = true;
            this.Opener.Click += new System.EventHandler(this.Opener_Click);
            // 
            // CreateTriangle
            // 
            this.CreateTriangle.Location = new System.Drawing.Point(13, 128);
            this.CreateTriangle.Name = "CreateTriangle";
            this.CreateTriangle.Size = new System.Drawing.Size(109, 23);
            this.CreateTriangle.TabIndex = 9;
            this.CreateTriangle.Text = "Eq. Triangle";
            this.CreateTriangle.UseVisualStyleBackColor = true;
            this.CreateTriangle.Click += new System.EventHandler(this.CreateTriangle_Click);
            // 
            // CreateSomethin
            // 
            this.CreateSomethin.Location = new System.Drawing.Point(12, 99);
            this.CreateSomethin.Name = "CreateSomethin";
            this.CreateSomethin.Size = new System.Drawing.Size(110, 23);
            this.CreateSomethin.TabIndex = 8;
            this.CreateSomethin.Text = "Something";
            this.CreateSomethin.UseVisualStyleBackColor = true;
            this.CreateSomethin.Click += new System.EventHandler(this.CreateSomethin_Click);
            // 
            // CreateRectangle
            // 
            this.CreateRectangle.Location = new System.Drawing.Point(13, 70);
            this.CreateRectangle.Name = "CreateRectangle";
            this.CreateRectangle.Size = new System.Drawing.Size(109, 23);
            this.CreateRectangle.TabIndex = 7;
            this.CreateRectangle.Text = "Rectangle";
            this.CreateRectangle.UseVisualStyleBackColor = true;
            this.CreateRectangle.Click += new System.EventHandler(this.CreateRectangle_Click);
            // 
            // Finisher
            // 
            this.Finisher.Location = new System.Drawing.Point(13, 41);
            this.Finisher.Name = "Finisher";
            this.Finisher.Size = new System.Drawing.Size(109, 23);
            this.Finisher.TabIndex = 6;
            this.Finisher.Text = "Finish";
            this.Finisher.UseVisualStyleBackColor = true;
            this.Finisher.Click += new System.EventHandler(this.Finisher_Click);
            // 
            // Lengther
            // 
            this.Lengther.Location = new System.Drawing.Point(13, 396);
            this.Lengther.Name = "Lengther";
            this.Lengther.Size = new System.Drawing.Size(109, 23);
            this.Lengther.TabIndex = 5;
            this.Lengther.Text = "Set Length";
            this.Lengther.UseVisualStyleBackColor = true;
            this.Lengther.Click += new System.EventHandler(this.Lengther_Click);
            // 
            // Verticaler
            // 
            this.Verticaler.Location = new System.Drawing.Point(13, 367);
            this.Verticaler.Name = "Verticaler";
            this.Verticaler.Size = new System.Drawing.Size(109, 23);
            this.Verticaler.TabIndex = 4;
            this.Verticaler.Text = "Set Vertical";
            this.Verticaler.UseVisualStyleBackColor = true;
            this.Verticaler.Click += new System.EventHandler(this.Verticaler_Click);
            // 
            // Horizonter
            // 
            this.Horizonter.Location = new System.Drawing.Point(13, 338);
            this.Horizonter.Name = "Horizonter";
            this.Horizonter.Size = new System.Drawing.Size(110, 23);
            this.Horizonter.TabIndex = 3;
            this.Horizonter.Text = "Set horizontal";
            this.Horizonter.UseVisualStyleBackColor = true;
            this.Horizonter.Click += new System.EventHandler(this.Horizonter_Click);
            // 
            // Disecter
            // 
            this.Disecter.Location = new System.Drawing.Point(13, 309);
            this.Disecter.Name = "Disecter";
            this.Disecter.Size = new System.Drawing.Size(110, 23);
            this.Disecter.TabIndex = 2;
            this.Disecter.Text = "Disect";
            this.Disecter.UseVisualStyleBackColor = true;
            this.Disecter.Click += new System.EventHandler(this.Disecter_Click);
            // 
            // Deleter
            // 
            this.Deleter.Location = new System.Drawing.Point(12, 225);
            this.Deleter.Name = "Deleter";
            this.Deleter.Size = new System.Drawing.Size(110, 23);
            this.Deleter.TabIndex = 1;
            this.Deleter.Text = "Delete";
            this.Deleter.UseVisualStyleBackColor = true;
            this.Deleter.Click += new System.EventHandler(this.Deleter_Click);
            // 
            // Cleaner
            // 
            this.Cleaner.Location = new System.Drawing.Point(12, 12);
            this.Cleaner.Name = "Cleaner";
            this.Cleaner.Size = new System.Drawing.Size(110, 23);
            this.Cleaner.TabIndex = 0;
            this.Cleaner.Text = "Clean";
            this.Cleaner.UseVisualStyleBackColor = true;
            this.Cleaner.Click += new System.EventHandler(this.Cleaner_Click);
            // 
            // Painter
            // 
            this.Painter.BackColor = System.Drawing.Color.White;
            this.Painter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Painter.Location = new System.Drawing.Point(0, 0);
            this.Painter.Name = "Painter";
            this.Painter.Size = new System.Drawing.Size(1240, 653);
            this.Painter.TabIndex = 0;
            this.Painter.TabStop = false;
            this.Painter.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Painter_MouseDown);
            this.Painter.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Painter_MouseMove);
            this.Painter.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Painter_MouseUp);
            // 
            // MessageLog
            // 
            this.MessageLog.AutoSize = true;
            this.MessageLog.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.MessageLog.Location = new System.Drawing.Point(0, 636);
            this.MessageLog.Name = "MessageLog";
            this.MessageLog.Size = new System.Drawing.Size(46, 17);
            this.MessageLog.TabIndex = 1;
            this.MessageLog.Text = "label1";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1382, 653);
            this.Controls.Add(this.MessageLog);
            this.Controls.Add(this.Splitter);
            this.Name = "MainWindow";
            this.Text = "PolygonEditor";
            this.Shown += new System.EventHandler(this.MainWindow_Shown);
            this.Splitter.Panel1.ResumeLayout(false);
            this.Splitter.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Splitter)).EndInit();
            this.Splitter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Painter)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer Splitter;
        private System.Windows.Forms.PictureBox Painter;
        private System.Windows.Forms.Button Deleter;
        private System.Windows.Forms.Button Cleaner;
        private System.Windows.Forms.Label MessageLog;
        private System.Windows.Forms.Button Lengther;
        private System.Windows.Forms.Button Verticaler;
        private System.Windows.Forms.Button Horizonter;
        private System.Windows.Forms.Button Disecter;
        private System.Windows.Forms.Button Finisher;
        private System.Windows.Forms.Button CreateRectangle;
        private System.Windows.Forms.Button CreateTriangle;
        private System.Windows.Forms.Button CreateSomethin;
        private System.Windows.Forms.Button Saver;
        private System.Windows.Forms.Button Opener;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

