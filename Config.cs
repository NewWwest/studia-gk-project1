﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolygonEditor
{
    public static class Config
    {
        public static readonly bool RandomIsFun = false;
        public static readonly bool UseEmbededLineAlgorythm = false;

        public static readonly int GuideLineHitBox = 10;
        public static readonly int EdgeHitBox = 20;
        public static readonly int VertexRadius = 10;
        public static readonly int PenWidth = 1;
        public static readonly float FontToRadiusRatio = 0.7f;

        public static readonly Color VertexBackground = Color.FromKnownColor(KnownColor.White);
        public static readonly Color SelectedVertexBackground = Color.FromKnownColor(KnownColor.MediumSpringGreen);
        public static readonly Color VertexColor = Color.FromKnownColor(KnownColor.Gray);

        public static readonly Color EdgeColor = Color.FromKnownColor(KnownColor.Gray);
        public static readonly Color SelectedEdgeColor = Color.FromKnownColor(KnownColor.MediumSpringGreen);

        public static readonly Color LabelColor = Color.FromKnownColor(KnownColor.Black);

    }
}
