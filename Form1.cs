﻿using PolygonEditor.Models;
using PolygonEditor.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolygonEditor
{
    public partial class MainWindow : Form
    {
        private Polygon polygon;
        private Logger logger;
        private Bitmap bitmap;
        private bool movingEnabled = false;

        public MainWindow()
        {
            InitializeComponent();
            logger = new Logger(MessageLog);
            polygon = new Polygon(logger);
            //NOt proud of this
            bitmap = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            Painter.BackgroundImage = bitmap;
            DoubleBuffered = true;
            logger.Message("Ready");
        }

        private void MainWindow_Shown(object sender, EventArgs e)
        {

        }



        private void Painter_MouseUp(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    if (polygon.FinishedConstruction)
                    {
                        movingEnabled = false;
                        Vertex v = polygon.SelectedVertex();
                        if (v != null)
                        {
                            foreach (var vertex in polygon.Vertecies)
                            {
                                if (vertex == v)
                                    continue;
                                if (Math.Abs(vertex.Location.X - v.Location.X) < Config.GuideLineHitBox)
                                {
                                    polygon.MoveVertex(new Point(vertex.Location.X, e.Location.Y));
                                    break;
                                }
                                else if (Math.Abs(vertex.Location.Y - v.Location.Y) < Config.GuideLineHitBox)
                                {
                                    polygon.MoveVertex(new Point(e.Location.X, vertex.Location.Y));
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        Vertex addedvertex = null;
                        foreach (var vertex in polygon.Vertecies)
                        {
                            if (Math.Abs(vertex.Location.X - e.Location.X) < Config.GuideLineHitBox)
                            {
                                addedvertex = polygon.AddVertex(new Point(vertex.Location.X, e.Location.Y));
                                break;
                            }
                            else if (Math.Abs(vertex.Location.Y - e.Location.Y) < Config.GuideLineHitBox)
                            {
                                addedvertex = polygon.AddVertex(new Point(e.Location.X, vertex.Location.Y));
                                break;
                            }
                        }
                        if(addedvertex==null)
                            polygon.AddVertex(e.Location);
                    }
                    Repaint();
                    break;
                case MouseButtons.Right:
                    if (polygon.FinishedConstruction)
                    {
                        polygon.Select(e.Location);
                        Repaint();
                    }
                    break;
                case MouseButtons.Middle:
                    Finisher_Click(null, null);
                    break;
                default:
                    break;
            }
        }

        private void Disecter_Click(object sender, EventArgs e)
        {
            polygon.DisectEdge();
            Repaint();
        }

        private void Horizonter_Click(object sender, EventArgs e)
        {
            polygon.SetHorizontal();
            Repaint();
        }

        private void Verticaler_Click(object sender, EventArgs e)
        {
            polygon.SetVertical();
            Repaint();
        }

        private void Lengther_Click(object sender, EventArgs e)
        {
            var dialog = new LengthDialog();
            var length = polygon.GetLength();
            if (!length.HasValue)
                return;

            dialog.Length = length.Value;
            dialog.Show();
            Enabled = false;
            dialog.Callback = () =>
            {
                Enabled = true;
                polygon.SetLength(dialog.Length);
                Repaint();
            };
        }

        private void Cleaner_Click(object sender, EventArgs e)
        {
            polygon = new Polygon(logger);
            Repaint();
            logger.Message("Polygon removed");
        }

        private void Deleter_Click(object sender, EventArgs e)
        {
            polygon.DeleteVertex();
            Repaint();
        }

        private void Repaint(bool withGuidelines = false, Point mouseLocation = default(Point))
        {
            try
            {
                PaintUtills.PaintPolygonFlood(bitmap, Config.FontToRadiusRatio, polygon, withGuidelines, mouseLocation);
                Painter.Refresh();
            }
            catch (Exception exc)
            {
                //logger.Message($"Something wrong: {exc.ToString()}");
                logger.Message("Something is wrong.  Make sure you didnt broke the polygon");
            }
        }

        private void Finisher_Click(object sender, EventArgs e)
        {
            polygon.FinishedConstruction = true;
            Repaint();
        }

        private void CreateSomethin_Click(object sender, EventArgs e)
        {
            polygon = new Polygon(logger);
            polygon.AddVertex(new Point(200, 150));
            polygon.AddVertex(new Point(200, 300));
            polygon.AddVertex(new Point(400, 150));
            polygon.AddVertex(new Point(200, 200));
            polygon.AddVertex(new Point(100, 300));
            polygon.AddVertex(new Point(300, 300));
            polygon.FinishedConstruction = true;
            polygon.Select(new Point(300, 300));
            polygon.SetHorizontal();
            polygon.Select(new Point(400, 200));
            polygon.SetVertical();
            polygon.Select(new Point(300, 150));
            polygon.SetHorizontal();
            polygon.Select(new Point(150, 250));
            polygon.SetLength(150);
            polygon.Select(new Point(200, 300));
            polygon.SetLength(300);
            polygon.Select(new Point(230, 250));
            polygon.SetLength(150);
            polygon.SetVertical();
            Repaint();
        }

        private void CreateTriangle_Click(object sender, EventArgs e)
        {
            polygon = new Polygon(logger);
            polygon.AddVertex(new Point(200, 200));
            polygon.AddVertex(new Point(100, 300));
            polygon.AddVertex(new Point(300, 300));
            polygon.FinishedConstruction = true;
            polygon.Select(new Point(150, 250));
            polygon.SetLength(150);
            polygon.Select(new Point(200, 300));
            polygon.SetLength(150);
            polygon.Select(new Point(230, 250));
            polygon.SetLength(150);
            Repaint();
        }

        private void CreateRectangle_Click(object sender, EventArgs e)
        {
            polygon = new Polygon(logger);
            polygon.AddVertex(new Point(200, 150));
            polygon.AddVertex(new Point(200, 300));
            polygon.AddVertex(new Point(400, 300));
            polygon.AddVertex(new Point(400, 150));
            polygon.FinishedConstruction = true;
            polygon.Select(new Point(300, 300));
            polygon.SetHorizontal();
            polygon.Select(new Point(400, 200));
            polygon.SetVertical();
            polygon.Select(new Point(300, 150));
            polygon.SetHorizontal();
            polygon.Select(new Point(200, 200));
            polygon.SetVertical();
            Repaint();

        }

        private void Painter_MouseMove(object sender, MouseEventArgs e)
        {
            if (polygon.FinishedConstruction && movingEnabled)
            {
                polygon.MoveVertex(e.Location);
            }
            Repaint(true, e.Location);
        }

        private void Painter_MouseDown(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    {
                        if (polygon.FinishedConstruction)
                            movingEnabled = true;
                        break;
                    }
            }
        }

        private void Opener_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                using (var streamReader = new StreamReader(openFileDialog1.OpenFile()))
                {
                    var serialized = streamReader.ReadToEnd();
                    polygon = Polygon.Deserialize(serialized, logger);
                    Repaint();
                }
            }
        }

        private void Saver_Click(object sender, EventArgs e)
        {
            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                using (var streamWriter = new StreamWriter(saveFileDialog1.OpenFile()))
                {
                    streamWriter.WriteLine(polygon.Serialize());
                }
            }
        }
    }
}
