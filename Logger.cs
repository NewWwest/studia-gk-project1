﻿using System.Windows.Forms;

namespace PolygonEditor
{
    internal class Logger
    {
        private Label messageLog;

        public Logger(Label messageLog)
        {
            this.messageLog = messageLog;
        }


        public void Message(string message)
        {
            messageLog.Text = $"Last Message: {message}";
        }
    }
}